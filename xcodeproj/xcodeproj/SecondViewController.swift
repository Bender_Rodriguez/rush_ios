//
//  SecondViewController.swift
//  xcodeproj
//
//  Created by swift on 19/12/14.
//  Copyright (c) 2014 swift. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class SecondViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate  {


    @IBOutlet weak var Space_V: UIButton!
    @IBOutlet weak var gps: UIButton!
    @IBOutlet weak var Map_Mode: UISegmentedControl!
    
    @IBOutlet weak var mapView: MKMapView!
    var manager:CLLocationManager!
   // var myLocations: [CLLocation] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        let location = CLLocationCoordinate2D(
            latitude: 48.89668380,
            longitude: 2.31837550
        )
        // 2
        let span = MKCoordinateSpanMake(0.001, 0.001)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        //3
        let annotation = MKPointAnnotation()
        annotation.setCoordinate(location)
        annotation.title = "Ecole 42"
        annotation.subtitle = "Paris, France"
        mapView.addAnnotation(annotation)        // Do any additional setup after loading the view, typically from a nib.
       
        //Setup our Location Manager
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Mode(sender: UISegmentedControl) {
        switch Map_Mode.selectedSegmentIndex
        {
        case 0:
            mapView.mapType=MKMapType.Standard;
        case 1:
            mapView.mapType=MKMapType.Satellite;
        case 2:
            mapView.mapType=MKMapType.Hybrid;
        default:
            break; 
        }
    }
    
       @IBAction func gps(sender: UIButton) {
        let span = MKCoordinateSpanMake(0.0005, 0.0005)
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
    }
    
    @IBAction func space(sender: UIButton) {
        let location = CLLocationCoordinate2D(
            latitude: 35.6895000,
            longitude: 139.6917100
        )
        // 2
        let span = MKCoordinateSpanMake(150, 150)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
     func locationManager(manager:CLLocationManager, didUpdateLocations locations:[AnyObject]) {
        let spanX = 0.007
        let spanY = 0.007
        var newRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpanMake(spanX, spanY))
        mapView.setRegion(newRegion, animated: true)
    }
}

